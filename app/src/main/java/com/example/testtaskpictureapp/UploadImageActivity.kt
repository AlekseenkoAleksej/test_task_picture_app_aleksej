package com.example.testtaskpictureapp

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.example.testtaskpictureapp.databinding.ActivityUploadImageBinding
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.storage
import java.io.ByteArrayOutputStream

class UploadImageActivity : AppCompatActivity() {

private lateinit var binding: ActivityUploadImageBinding
private var launcher: ActivityResultLauncher<Intent>? = null
private var selectedImage: Uri? = null
private var uploadUri: Uri? = null
lateinit var storage: FirebaseStorage
lateinit var storageRef: StorageReference
private lateinit var auth: FirebaseAuth

override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityUploadImageBinding.inflate(layoutInflater)
    setContentView(binding.root)

    init()
}

private fun init() {
    // выбор изображения из галереи телефона
    selectGalleryImage()

    auth = Firebase.auth
    val currentUser = auth.currentUser
    storage = Firebase.storage
    storageRef = storage.getReference("Image")

    // проверяем, зарегистрирован ли пользователь, если да - то показываем его емейл
    if (currentUser != null) {
        val userName = currentUser.email
        binding.tvUserEmail.text = userName
    }
    // сохраняем изображение на Firebase Storage по нажатию на кнопку SaveImage
    binding.bSaveImage.setOnClickListener { uploadSaveImage() }

    // выходим из своей учетной записи по нажатию на кнопку SignOut
    binding.bSignOut.setOnClickListener {
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this@UploadImageActivity, LoginActivity::class.java))
    }
// кнопка для перехода в галерею из сохраненных нами изображений
    binding.bGallery.setOnClickListener {
        startActivity(Intent(this@UploadImageActivity, ShowAllImagesFromStorage::class.java))
    }

}
// по нажатию на изображение открывается галерея телефона, где выбираем изображение для загрузки на Firebase Storage
private fun selectGalleryImage() {
    launcher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult())
    {
            result: ActivityResult ->
        if (result.resultCode == RESULT_OK) {
            selectedImage = result.data?.data
            binding.imageView.setImageURI(selectedImage)
        }
    }
binding.imageView.setOnClickListener {
    launcher?.launch(Intent( Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI))
}
}
// сохраняем выбранное изображение на Firebase Storage, для этого превращаем его в поток байтов,
// название/путь изображения генерируем из "image_" и текущего времени в милисекундах, чтобы названия
// загружаемых изображений были рандомными
private fun uploadSaveImage() {
    val bitmap = (binding.imageView.drawable as BitmapDrawable).bitmap
    val baos = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
    val data = baos.toByteArray()
    val childStorageRef = storageRef.child("image_" + System.currentTimeMillis())
    val uploadTask = childStorageRef.putBytes(data)

uploadTask.continueWithTask { task ->
    if (!task.isSuccessful) {
        task.exception?.let {
            throw it
        }
    }
    childStorageRef.downloadUrl
}.addOnCompleteListener { task ->
    if (task.isSuccessful) {
        // Ссылка на загруженное в облако фото
        uploadUri = task.result
        shortToast(this, "Upload image successfull")
    }
}
}
}