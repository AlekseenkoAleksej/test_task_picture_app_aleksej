package com.example.testtaskpictureapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.testtaskpictureapp.databinding.ActivityShowImageBinding

class ShowImageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShowImageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityShowImageBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

//принимаем ссылку на изображение из ShowAllImagesFromStorage activity
//c помощью созданного класса ImageLoader показываем изображение в ImageView
        val imageUri = intent.getStringExtra(Constants.IMAGE_URI)

        Glide
            .with(this)
            .load(imageUri)
            .error(R.drawable.load)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.color.grey)
            .into(binding.ivImageUri)
    }
}