package com.example.testtaskpictureapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import java.util.*

class ImageAdapter(var listener: Listener, private val imageList: ArrayList<String>) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {

// надуваем разметку одного элемента, этими элементами адаптер будет заполнять RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return ViewHolder(view)
    }

// используем класс ImageLoader для загрузки изображений из интернета
// в него отправляем ссылку на изображение и где разместить
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    Glide
        .with(holder.itemView.context)
        .load(imageList[position])
        .error(R.drawable.load)
        .sizeMultiplier(0.6f)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .placeholder(R.color.grey)
        .into(holder.imageView)
    }

// адаптеру нужно знать количество элементов в списке
    override fun getItemCount(): Int {
        return imageList.size
    }

// адаптер заполняет одну ячейку RecyclerView, а именно помещает в ячейку
// взятое с помощью библиотеки Glide одно изображение
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView

        init {
            imageView = itemView.findViewById(R.id.item)
            imageView.setOnClickListener {
                listener.onClick(imageList[adapterPosition])

            }
        }
    }
//для прослушивания кликов по изображению
    interface Listener{
        fun onClick(imageUri: String)
    }
}