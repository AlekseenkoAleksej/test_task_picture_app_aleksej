package com.example.testtaskpictureapp

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import com.example.testtaskpictureapp.databinding.ActivityLoginBinding
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth

class LoginActivity : AppCompatActivity() {

private lateinit var binding: ActivityLoginBinding
var email = ""
var password = ""
private lateinit var auth: FirebaseAuth

public override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityLoginBinding.inflate(layoutInflater)
    setContentView(binding.root)
    auth = Firebase.auth
    init()
}

public override fun onStart() {
    super.onStart()

// проверяем, зарегистрирован ли пользователь, если да - сразу переходим на страницу загрузки изображений
// если нет - остаемся тут
    val currentUser = auth.currentUser
    if (currentUser != null) {
            goToUploadImageActivity()
    }
}

private fun init() {
    binding.bSignUp.setOnClickListener {
        userRegistration()
    }
    binding.bSignIn.setOnClickListener {
        userAuthentication()
    }
}

// проверяем, заполнены ли все поля, зарегистрирован ли пользователь
// создаем аккаунт и отправляем письмо подтверждения емейла
private fun userRegistration() {
    if(!TextUtils.isEmpty(binding.etEmail.text.toString()) && !TextUtils.isEmpty(binding.etPassword.text.toString()))
    {
        email = binding.etEmail.text.toString()
        password = binding.etPassword.text.toString()
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    sendEmailVerification()
                } else {
                    shortToast(this, "User Registration failed")
                }
            }
    }
    else
    {
        shortToast(this, "Please enter Email and Password")
    }
}

// вход в зарегистрированный ранее аккаунт, при успешном входе - переход на страницу загрузки изображений
private fun userAuthentication() {
    if(!TextUtils.isEmpty(binding.etEmail.text.toString()) && !TextUtils.isEmpty(binding.etPassword.text.toString()))
    {
        email = binding.etEmail.text.toString()
        password = binding.etPassword.text.toString()
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    goToUploadImageActivity()
                    shortToast(this, "User Authentication Successful")
                } else {
                    shortToast(this, "User Authentication failed")
                }
            }
      }
      else
      {
          shortToast(this, "Please enter Email and Password")
      }
}

private fun sendEmailVerification() {
    val user = auth.currentUser!!
    user.sendEmailVerification()
        .addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                longToast(this, "Check your email and confirm registration")
            }
            else {
                longToast(this, "Send email failed")
            }
        }
}

private fun goToUploadImageActivity()
{
    startActivity(Intent(this@LoginActivity, UploadImageActivity::class.java))
}
}