package com.example.testtaskpictureapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.example.testtaskpictureapp.databinding.ActivityShowAllImagesFromStorageBinding
import com.google.firebase.Firebase
import com.google.firebase.storage.component1
import com.google.firebase.storage.component2
import com.google.firebase.storage.component3
import com.google.firebase.storage.storage

class ShowAllImagesFromStorage : AppCompatActivity(), ImageAdapter.Listener {

    lateinit var binding: ActivityShowAllImagesFromStorageBinding
    var imagelist: ArrayList<String>? = null
    var adapter: ImageAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityShowAllImagesFromStorageBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        imagelist = ArrayList()
        adapter = ImageAdapter(this,imagelist!!)
        binding.recyclerview.layoutManager = GridLayoutManager(this@ShowAllImagesFromStorage, 2)
        listAllPaginated("")
}
// используем постраничную загрузку изображений в галерею с Firebase Storage, я здесь поставил просто maxResults по 1.
// здесь имеется ввиду галерея - это recyclerview, в который adapter помещает изображения, и получается галерея
    private fun listAllPaginated(pageToken: String?) {
        val storage = Firebase.storage
        val listRef = storage.reference.child("Image")

        val listPageTask = if (pageToken != null) {
            listRef.list(1, pageToken)
        } else {
            listRef.list(1)
        }
        listPageTask
            .addOnSuccessListener { (items, _, pageToken) ->
                for (file in items) {
                    file.downloadUrl.addOnSuccessListener { uri ->
                        imagelist!!.add(uri.toString())
                    }.addOnSuccessListener {
                        binding.recyclerview.adapter = adapter
                    }
                    pageToken?.let {
                        listAllPaginated(it)
                    }
                }
            }
    }

// по нажатию на изображение ссылка на него (ссылка из Firebase Storage) отправляется на другое активити,
// на котором показывается целое изображение
    override fun onClick(imageUri: String) {
        startActivity(Intent(this@ShowAllImagesFromStorage, ShowImageActivity::class.java).apply {
            putExtra(Constants.IMAGE_URI, imageUri)
        })
    }
}